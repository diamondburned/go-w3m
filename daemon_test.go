package w3m

import (
	"testing"
	"time"
)

func TestDaemonize(t *testing.T) {
	a := &Arguments{
		Width:  300,
		Height: 245,
	}

	for i := 0; i < 6; i++ {
		var d *Daemon

		if (i % 2) == 0 {
			d = Daemonize(a, "_testdata/Tateyama.Ayano.full.1795995.jpg")
		} else {
			d = Daemonize(a, "_testdata/1e64205547f6a27259e486ffc113f3eb.jpg")
		}

		time.Sleep(200 * time.Millisecond)
		if d.LastError != nil {
			t.Fatal(d.LastError)
		}

		d.Stop()
	}

	if err := ClearAll(); err != nil {
		t.Fatal(err)
	}
}
