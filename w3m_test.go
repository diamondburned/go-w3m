package w3m

import (
	"testing"
	"time"
)

func TestSpawn(t *testing.T) {
	a := &Arguments{
		Width:  300,
		Height: 245,
	}

	if err := Spawn(a, "_testdata/Tateyama.Ayano.full.1795995.jpg"); err != nil {
		t.Fatal(err)
	}

	time.Sleep(time.Second * 3)

	if err := ClearAll(); err != nil {
		t.Fatal(err)
	}
}

