package w3m

// Daemon is the struct for daemonizing
type Daemon struct {
	LastError error
	stop      chan struct{}
}

// Daemonize runs w3m in a background loop. This function
func Daemonize(a *Arguments, filename string) *Daemon {
	d := &Daemon{
		stop: make(chan struct{}),
	}

	go func() {
		for {
			select {
			case <-d.stop:
				return
			default:
				if err := Spawn(a, filename); err != nil {
					d.LastError = err
				}
			}
		}
	}()

	return d
}

// Stop stops the daemon
func (d *Daemon) Stop() {
	d.stop <- struct{}{}
}
