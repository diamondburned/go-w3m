package w3m

import (
	"errors"
	"fmt"
	"os/exec"
	"strings"
)

var (
	w3mpath string

	// ErrNotFound is returned when w3m can't be found
	ErrNotFound = errors.New("w3m not found")

	// ErrNoArguments is returned when one of the arguments are missing
	ErrNoArguments = errors.New("No arguments provided")
)

// Arguments is the struct for w3m arguments
// All fields are required
type Arguments struct {
	Xoffset int // default: 0
	Yoffset int // default: 0
	Width   int
	Height  int
}

// Spawn spawns a w3m image
func Spawn(a *Arguments, filename string) error {
	if w3mpath == "" {
		return ErrNotFound
	}

	if a == nil || filename == "" {
		return ErrNoArguments
	}

	var (
		cmd  = exec.Command(w3mpath)
		args = fmt.Sprintf(
			"0;1;%d;%d;%d;%d;;;;;%s\n3;\n4\n",
			a.Xoffset, a.Yoffset,
			a.Width, a.Height,
			filename,
		)
	)

	reader := strings.NewReader(args)
	cmd.Stdin = reader
	cmd.Run()

	return nil
}

// Clear clears the image
func Clear(a *Arguments) error {
	if w3mpath == "" {
		return ErrNotFound
	}

	if a == nil {
		return ErrNoArguments
	}

	var (
		cmd  = exec.Command(w3mpath)
		args = fmt.Sprintf(
			"6;%d;%d;%d;%d\n3;",
			a.Xoffset, a.Yoffset,
			a.Width, a.Height,
		)
	)

	reader := strings.NewReader(args)
	cmd.Stdin = reader
	cmd.Run()

	return nil
}

// ClearAll clears all the images on the terminal
// This works by clearing a frame 9999x9999px large
func ClearAll() error {
	if w3mpath == "" {
		return ErrNotFound
	}

	var (
		cmd    = exec.Command(w3mpath)
		reader = strings.NewReader("6;0;0;9999;9999\n3;")
	)

	cmd.Stdin = reader
	cmd.Run()

	return nil
}
